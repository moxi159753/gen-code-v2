package v1

import (
	"github.com/gogf/gf/v2/frame/g"
)

type GetInfoReq struct {
	g.Meta `path:"/getInfo" tags:"Index" method:"get" summary:"获取用户信息"`
}

type GetRoutersReq struct {
	g.Meta `path:"/getRouters" tags:"Index" method:"get" summary:"获取路由"`
}
