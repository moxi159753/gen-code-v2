package main

import (
	"gen-code-v2/internal/cmd"
	_ "gen-code-v2/internal/packed"
	_ "github.com/gogf/gf/contrib/drivers/mssql/v2"
	//_ "github.com/gogf/gf/contrib/drivers/oracle/v2"
	_ "github.com/gogf/gf/contrib/drivers/pgsql/v2"
	"github.com/gogf/gf/v2/os/gctx"
)

func main() {
	cmd.Main.Run(gctx.New())
}
