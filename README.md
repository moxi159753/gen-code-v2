<h1 align="center">GEN-CODE 代码生成器</h1>


<p align=center>
   基于GoFrame框架的代码生成器
</p>
<p align="center">
<a target="_blank" href="https://gitee.com/genCode/gen-code-v2">
    <img src="https://gitee.com/genCode/gen-code-v2/badge/star.svg?theme=dark" alt="star"/>
    <img src="https://gitee.com/genCode/gen-code-v2/badge/fork.svg?theme=dark" alt="fork"/>
</a>
</p>

# 介绍

- 后端使用GoFrame框架编写: 框架文档：[戳这](https://goframe.org/)，
- 前端基于Vben Admin前端框架: 框架文档：[戳这](https://vvbin.cn/doc-next/)
- 感谢大佬的CRUD思想，EL-ADMIN：[戳这](https://gitee.com/elunez/eladmin-web)

# 项目源码
- 后端：[https://gitee.com/genCode/gen-code-v2](https://gitee.com/genCode/gen-code-v2)
- 前端：[https://gitee.com/genCode/gen-code-web](https://gitee.com/genCode/gen-code-web)


# 特征

- 多数据源：动态添加数据源，导入表信息生成代码
- 自定义模板: 支持在线维护代码模板，按需设置模板
- 自定义属性：除表信息之外，还可设置额外属性，提高模板局限性

# 内置功能

1. 数据源管理：维护数据源，代码生成可导入不同数据库的表结构
2. 代码生成器：通过表结构信息生成代码
    - 导入：从数据库导入表
    - 创建：通过DDL创建表语句生成
    - 预览：预览生成的代码，可预览不同模板
    - 生成：生成代码下载，支持多模板同时生成
3. 模板管理：在线管理模板，编辑器支持内置模板变量语法提示
    - 编辑器： [monaco-editor](https://github.com/microsoft/monaco-editor) 
    - 树表格： [vxe-table](https://gitee.com/xuliangzhan_admin/vxe-table)

# [演示地址](http://gencode.gitee.io/gen-code-admin)

# 演示图

<table>
    <tr>
        <td><img src="./doc/images/datasource.png" alt="datasource"/></td>
        <td><img src="./doc/images/table.png" alt="table"/></td>
    </tr>
    <tr>
        <td><img src="./doc/images/importTable.png" alt="importTable"/></td>
        <td><img src="./doc/images/preview.png" alt="preview"/></td>
    </tr>
    <tr>
        <td><img src="./doc/images/template.png" alt="template"/></td>
        <td><img src="./doc/images/editTpl.png" alt="editTpl"/></td>
    </tr>
</table>

