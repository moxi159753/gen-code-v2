<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE mapper PUBLIC "-//mybatis.org//DTD Mapper 3.0//EN" "http://mybatis.org/dtd/mybatis-3-mapper.dtd">
<mapper namespace="{{.Table.PackageName}}.mapper.{{.Table.ClassName}}Mapper">
    
    <resultMap id="{{.Table.ClassName}}Result" type="{{.Table.PackageName}}.model.{{.Table.ClassName}}">
{{range $index, $column := .Table.Columns}}
        <result property="{{$column.JavaField}}"    column="{{$column.ColumnName}}"    />
{{end}}

    </resultMap>
{{if eq .Table.TplCategory "sub"}}

    <resultMap id="{{.Table.ClassName}}{{.Table.SubTable.ClassName}}Result" type="{{.Table.PackageName}}.model.vo.{{.Table.ClassName}}VO" extends="{{.Table.ClassName}}Result">
        <collection property="{{.Table.SubTable.ClassName | CaseCamelLower }}List" notNullColumn="sub_{{.Table.SubTable.PkColumn.ColumnName}}" javaType="java.util.List" resultMap="{{.Table.SubTable.ClassName}}Result" />
    </resultMap>

    <resultMap type="{{.Table.PackageName}}.model.{{.Table.SubTable.ClassName}}" id="{{.Table.SubTable.ClassName}}Result">
        {{range $index, $column := .Table.SubTable.Columns}}
            <result property="{{$column.JavaField}}"    column="{{$column.ColumnName}}"    />
        {{end}}
    </resultMap>
{{end}}
{{ $length := len .Table.Columns }}
    <sql id="select{{.Table.ClassName}}Vo">
        select{{range $index, $column := .Table.Columns}} `{{$column.ColumnName}}`{{if ne (Sum $index 1) $length}},{{end}}{{end}} from `{{.Table.TableName}}`
    </sql>

    <select id="select{{.Table.ClassName}}List" parameterType="{{.Table.PackageName}}.model.{{.Table.ClassName}}" resultMap="{{.Table.ClassName}}Result">
        <include refid="select{{.Table.ClassName}}Vo"/>
        <where>
{{range $index, $column := .Table.Columns}}
{{$queryType := $column.QueryType}}
{{$javaField := $column.JavaField}}
{{$javaType := $column.JavaType}}
{{$columnName := $column.ColumnName}}
{{$AttrName := ($column.JavaField | CaseCamel)}}
{{if eq $column.IsQuery "1"}}
{{if eq $column.QueryType "EQ"}}
            <if test="{{$javaField}} != null {{if eq $javaType "String"}} and {{$javaField}}.trim() != '' {{end}} "> and
                `{{$columnName}}` = #{ {{- $javaField -}} }</if>
{{else if eq $column.QueryType "NE"}}
            <if test="{{$javaField}} != null {{if eq $javaType "String"}} and {{$javaField}}.trim() != '' {{end}} "> and
        `{{$columnName}}` != #{ {{- $javaField -}} }</if>
{{else if eq $column.QueryType "GT"}}
            <if test="{{$javaField}} != null {{if eq $javaType "String"}} and {{$javaField}}.trim() != '' {{end}} "> and
        `{{$columnName}}` &gt; #{ {{- $javaField -}} }</if>
{{else if eq $column.QueryType "GTE"}}
            <if test="{{$javaField}} != null {{if eq $javaType "String"}} and {{$javaField}}.trim() != '' {{end}} "> and
        `{{$columnName}}` &gt;= #{ {{- $javaField -}} }</if>
{{else if eq $column.QueryType "LT"}}
            <if test="{{$javaField}} != null {{if eq $javaType "String"}} and {{$javaField}}.trim() != '' {{end}} "> and
        `{{$columnName}}` &lt; #{ {{- $javaField -}} }</if>
{{else if eq $column.QueryType "LTE"}}
            <if test="{{$javaField}} != null {{if eq $javaType "String"}} and {{$javaField}}.trim() != '' {{end}} "> and
        `{{$columnName}}` &lt;= #{ {{- $javaField -}} }</if>
{{else if eq $column.QueryType "LIKE"}}
            <if test="{{$javaField}} != null {{if eq $javaType "String"}} and {{$javaField}}.trim() != '' {{end}} "> and
        `{{$columnName}}` like concat('%','#{ {{- $javaField -}} }','%') </if>
{{end}}
{{end}}
{{end}}
        </where>
    </select>


    <insert id="batch{{.Table.ClassName}}">
        insert into {{.Table.TableName}}
        <trim prefix="(" suffix=")" suffixOverrides=",">
        {{range $index, $column := .Table.Columns}}
        {{$column.ColumnName}},
        {{end}}
        </trim>
        values
        <foreach item="item" index="index" collection="list">
        <trim prefix="(" suffix=")" suffixOverrides=",">
            {{range $index, $column := .Table.Columns}}
            #{item.{{- $column.JavaField -}},
            {{end}}
         </trim>
        </foreach>

    </insert>


</mapper>