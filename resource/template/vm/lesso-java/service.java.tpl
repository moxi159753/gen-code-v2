package {{.Table.PackageName}}.service;
////
import java.util.List;
import com.baomidou.mybatisplus.plugins.Page;
import {{.Table.PackageName}}.common.model.ServiceResultModel;
import {{.Table.PackageName}}.common.model.ResultServiceModel;
import {{.Table.PackageName}}.model.{{.Table.ClassName}};
import {{.Table.PackageName}}.model.vo.{{.Table.ClassName}}VO;
import {{.Table.PackageName}}.model.dto.{{.Table.ClassName}}DTO;
import top.ibase4j.core.base.IBaseService;
import top.ibase4j.core.exception.ServiceException;
import top.ibase4j.model.Pagination;
////
import java.util.List;
import java.util.Map;
////

/**
 * {{.Table.FunctionName}}Service接口
 * 
 * @author {{.Table.FunctionAuthor}}
 * @since {{.Table.CreateTime}}
 */
public interface I{{.Table.ClassName}}Service extends IBaseService<{{.Table.ClassName}}>{
////
     /**
      * 分页查询
      *
      * @param pagination 查询条件
      * @return
     */
    Page<{{.Table.ClassName}}VO> getPageList(Pagination<{{.Table.ClassName}}DTO> pagination) throws ServiceException;
////

    /**
    * 查询{{.Table.FunctionName}}列表
    *
    * @param {{ .Table.ClassName |CaseCamelLower}} {{.Table.FunctionName}}
    * @return {{.Table.FunctionName}}集合
    */
    List<{{.Table.ClassName}}VO> getList({{.Table.ClassName}} {{ .Table.ClassName |CaseCamelLower}}) throws ServiceException;
////

     /**
      * 查询{{.Table.FunctionName}}详情
      *
      * @param id id
      * @return {{.Table.FunctionName}}
     */
     ServiceResultModel<{{.Table.ClassName}}VO> getById({{.Table.PkColumn.JavaType}} id) throws ServiceException;
////
    /**
     * 添加{{.Table.FunctionName}}
     * @param model {{.Table.FunctionName}}信息
     * @return result
     * @throws ServiceException
    */
    ServiceResultModel add({{.Table.ClassName}} model) throws ServiceException;
////
    /**
     * 修改{{.Table.FunctionName}}
     * @param model {{.Table.FunctionName}}信息
     * @return result
     * @throws ServiceException
    */
    ServiceResultModel modify({{.Table.ClassName}} model) throws ServiceException;
////

    /**
     * 删除{{.Table.FunctionName}}记录
     * @param id  {{.Table.FunctionName}}ID
     * @return result
    */
    ServiceResultModel delete({{.Table.PkColumn.JavaType}} id);
////
    /**
     * 批量删除{{.Table.FunctionName}}记录
     * 
     * @param ids 需要删除的{{.Table.FunctionName}}主键集合
     * @return result
     */
     ServiceResultModel batchDelete(List<{{.Table.PkColumn.JavaType}}> ids);
////
     /**
      * 批量禁用{{.Table.FunctionName}}记录
      *
      * @param ids 需要启用的{{.Table.FunctionName}}主键集合
      * @return result
     */
     ServiceResultModel batchDisabled(List<{{.Table.PkColumn.JavaType}}> ids);
////
     /**
      * 批量启用{{.Table.FunctionName}}记录
      *
      * @param ids 需要启用的{{.Table.FunctionName}}主键集合
      * @return result
     */
     ServiceResultModel batchEnabled(List<{{.Table.PkColumn.JavaType}}> ids);

}