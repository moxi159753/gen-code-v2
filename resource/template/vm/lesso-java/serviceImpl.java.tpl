package {{.Table.PackageName}}.service.impl;
////
import java.util.List;
import java.util.ArrayList;
////
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import {{.Table.PackageName}}.common.model.ServiceResultModel;
import com.baomidou.mybatisplus.mapper.Wrapper;
import top.ibase4j.core.base.BaseService;
import {{.Table.PackageName}}.util.BeanConversionUtils;
import {{.Table.PackageName}}.util.ServiceResultUtil;
import {{.Table.PackageName}}.util.WrapperUtil;
import org.apache.dubbo.config.annotation.DubboService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import {{.Table.PackageName}}.mapper.{{.Table.ClassName}}Mapper;
import {{.Table.PackageName}}.service.I{{.Table.ClassName}}Service;
import {{.Table.PackageName}}.model.{{.Table.ClassName}};
import {{.Table.PackageName}}.model.vo.{{.Table.ClassName}}VO;
import {{.Table.PackageName}}.model.dto.{{.Table.ClassName}}DTO;
import top.ibase4j.core.exception.ServiceException;
import top.ibase4j.model.Pagination;
////
/**
 * {{.Table.FunctionName}}Service业务层处理
 *
 * @author {{.Table.FunctionAuthor}}
 * @since {{.Table.CreateTime}}
*/
@Component
@DubboService(interfaceClass = I{{.Table.ClassName}}Service.class)
public class {{.Table.ClassName}}ServiceImpl extends BaseService<{{.Table.ClassName}}, {{.Table.ClassName}}Mapper> implements I{{.Table.ClassName}}Service{
////

    /**
     * 分页查询
     *
     * @param pagination 查询条件
     * @return
    */
    @Override
    public Page<{{.Table.ClassName}}VO> getPageList(Pagination<{{.Table.ClassName}}DTO> pagination) throws ServiceException {
        {{.Table.ClassName}}DTO entity = pagination.getEntity();
        Wrapper<{{.Table.ClassName}}> wrapper = new EntityWrapper<>();
        Pagination<{{.Table.ClassName}}> page = BeanConversionUtils.conversionObj(pagination, Pagination.class);
        WrapperUtil.getCondition(page, this.currentModelClass(), wrapper);
        Page<{{.Table.ClassName}}VO> pageParam = new Page(pagination.getCurrent(), pagination.getSize());
        List<{{.Table.ClassName}}> entityList = this.mapper.selectPage(pageParam, wrapper);
        pageParam.setRecords(BeanConversionUtils.conversionList(entityList, {{.Table.ClassName}}VO.class));
        return pageParam;
    }
////
    /**
     * 查询{{.Table.FunctionName}}列表
     *
     * @param {{ .Table.ClassName |CaseCamelLower}} {{.Table.FunctionName}}
     * @return {{.Table.FunctionName}}集合
    */
    @Override
    public List<{{.Table.ClassName}}VO> getList({{.Table.ClassName}} {{ .Table.ClassName |CaseCamelLower}}) throws ServiceException{
        Wrapper<{{.Table.ClassName}}> wrapper = WrapperUtil.getCondition({{ .Table.ClassName |CaseCamelLower}}, {{.Table.ClassName}}.class);
        List<{{.Table.ClassName}}> entityList = this.mapper.selectList(wrapper);
        return BeanConversionUtils.conversionList(entityList, {{.Table.ClassName}}VO.class);
    }
////
     /**
      * 查询{{.Table.FunctionName}}详情
      *
      * @param id id
      * @return {{.Table.FunctionName}}
     */
    @Override
    public ServiceResultModel<{{.Table.ClassName}}VO> getById({{.Table.PkColumn.JavaType}} id) throws ServiceException {
        {{.Table.ClassName}} model = this.mapper.selectById(id);
        {{.Table.ClassName}}VO modelVO = BeanConversionUtils.conversionObj(model, {{.Table.ClassName}}VO.class);
        return ServiceResultUtil.success(1L, modelVO);
    }
////
    /**
     * 添加{{.Table.FunctionName}}
     * @param model {{.Table.FunctionName}}信息
     * @return result
     * @throws ServiceException
    */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public ServiceResultModel add({{.Table.ClassName}} model) throws ServiceException{
        // TODO addLogic
        this.save(model);
        return ServiceResultUtil.success(1L, model);
    }
////
    /**
     * 修改{{.Table.FunctionName}}
     * @param model {{.Table.FunctionName}}信息
     * @return result
     * @throws ServiceException
    */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public ServiceResultModel modify({{.Table.ClassName}} model) throws ServiceException{
        // TODO modifyLogic
        this.mapper.updateById(model);
        return ServiceResultUtil.success("更新成功");
    }
////
    /**
     * 删除{{.Table.FunctionName}}记录
     * @param id  {{.Table.FunctionName}}ID
     * @return result
    */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public ServiceResultModel delete({{.Table.PkColumn.JavaType}} id) {
        this.mapper.deleteById(id);
        return ServiceResultUtil.success("删除成功");
    }
////
    /**
     * 批量删除{{.Table.FunctionName}}记录
     *
     * @param ids 需要删除的{{.Table.FunctionName}}主键集合
     * @return result
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public ServiceResultModel batchDelete(List<{{.Table.PkColumn.JavaType}}> ids) {
        this.mapper.deleteBatchIds(ids);
        return ServiceResultUtil.success("删除成功");
    }
////
     /**
      * 批量禁用{{.Table.FunctionName}}记录
      *
      * @param ids 需要启用的{{.Table.FunctionName}}主键集合
      * @return result
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public ServiceResultModel batchDisabled(List<{{.Table.PkColumn.JavaType}}> ids) {
        updateState(ids, 1);
        return ServiceResultUtil.success("禁用成功");
    }
////
     /**
      * 批量启用{{.Table.FunctionName}}记录
      *
      * @param ids 需要启用的{{.Table.FunctionName}}主键集合
      * @return result
     */
    @Override
    public ServiceResultModel batchEnabled(List<{{.Table.PkColumn.JavaType}}> ids) {
        updateState(ids, 0);
        return ServiceResultUtil.success("启用成功");
    }
////
   private void updateState(List<{{.Table.PkColumn.JavaType}}> ids, Integer state) {
       {{.Table.ClassName}} entity = new {{.Table.ClassName}}();
       entity.setState(state);
       Wrapper<{{.Table.ClassName}}> wrapper = new EntityWrapper<>();
       wrapper.in({{.Table.ClassName}}.COL_{{.Table.PkColumn.ColumnName| SnakeScreamingCase}}, ids);
       this.mapper.update(entity, wrapper);
   }

}
