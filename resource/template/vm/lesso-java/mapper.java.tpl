package {{.Table.PackageName}}.mapper;
////
import java.util.List;
import {{.Table.PackageName}}.model.{{.Table.ClassName}};
{{if eq .Table.TplCategory "sub"}}
import {{.Table.PackageName}}.model.{{.Table.SubTable.ClassName}};
{{end}}
import top.ibase4j.core.base.BaseMapper;
////
/**
 * {{.Table.FunctionName}}Mapper接口
 *
 * @author {{.Table.FunctionAuthor}}
 * @since {{.Table.CreateTime}}
 */
public interface {{.Table.ClassName}}Mapper extends BaseMapper<{{.Table.ClassName}}>
{
////
    /**
     * 查询{{.Table.FunctionName}}列表
     * 
     * @param {{ .Table.ClassName |CaseCamelLower}} {{.Table.FunctionName}}
     * @return {{.Table.FunctionName}}集合
     */
    public List<{{.Table.ClassName}}> select{{.Table.ClassName}}List({{.Table.ClassName}} {{ .Table.ClassName |CaseCamelLower}});
////
    /**
     * 批量新增${subTable.functionName}
     *
     * @param {{.Table.ClassName |CaseCamelLower}}List {{.Table.FunctionName}}列表
     * @return 结果
    */
    public int batch{{.Table.ClassName}}(@Param("list") List<{{.Table.ClassName}}> {{.Table.ClassName |CaseCamelLower}}List);
////

}
