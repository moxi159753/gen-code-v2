package {{.Table.PackageName}}.model;
////

{{range $index, $import := .ImportList}}
import {{$import}};
{{end}}

import java.io.Serializable;
import java.util.Collection;
import {{.Table.PackageName}}.common.model.BasisModel;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import javax.validation.constraints.Size;

import lombok.Data;
////
/**
* {{.Table.FunctionName}}对象 {{.Table.TableName}}
*
* @author {{.Table.FunctionAuthor}}
* @since {{.Table.CreateTime}}
*/
@ApiModel("{{.Table.TableComment}}")
@Data
@TableName("{{.Table.TableName}}")
public class {{.Table.ClassName}} extends BasisModel<{{.Table.ClassName}}>{
////
    private static final long serialVersionUID = 1L;


{{range $index, $column := .Table.Columns}}
////
  {{if not  (InArray PublicFields $column.ColumnName)}}
    /**
    *  Java类型:{{ $column.JavaType }}
    *  数据库类型:{{ $column.ColumnType }}
    *  注释:{{ $column.ColumnComment }}
    */
    {{if eq $column.JavaType "Long"}}
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    {{end}}
    @TableField(value = "{{$column.ColumnName}}")
    @ApiModelProperty(value = "{{$column.ColumnComment}}")
    {{if eq $column.IsPk "1"}}
    @TableId(value = "{{$column.ColumnName}}", type= IdType.AUTO)
    {{end}}
    private {{$column.JavaType}} {{$column.JavaField}};


  {{end}}
{{end}}


{{range $index, $column := .Table.Columns}}
  {{if not  (InArray PublicFields $column.ColumnName)}}
    /**
     * {{ $column.ColumnComment }}
    */
    public static final String COL_{{$column.ColumnName| SnakeScreamingCase}} = "{{$column.ColumnName}}";


  {{end}}
{{end}}
////
    @Override
    protected Serializable pkVal() {
        return this.{{.Table.PkColumn.JavaField}};
    }
////
    @Override
    protected Collection<? extends Serializable> pkVals() {
        return null;
    }

}
