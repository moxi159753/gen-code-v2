package {{.Table.PackageName}}.web;
////
import com.baomidou.mybatisplus.plugins.Page;
import {{.Table.PackageName}}.util.ServiceResultUtil;
import top.ibase4j.core.base.result.QueryResultModel;
import top.ibase4j.core.base.result.ResultModel;
import top.ibase4j.core.base.result.ResultUtil;
import top.ibase4j.core.exception.ServiceException;
import top.ibase4j.model.Pagination;
import {{.Table.PackageName}}.model.{{.Table.ClassName}};
import {{.Table.PackageName}}.model.vo.{{.Table.ClassName}}VO;
import {{.Table.PackageName}}.model.dto.{{.Table.ClassName}}DTO;
import {{.Table.PackageName}}.service.I{{.Table.ClassName}}Service;
import {{.Table.PackageName}}.validation.AddGroup;
import {{.Table.PackageName}}.validation.QueryGroup;
import {{.Table.PackageName}}.validation.UpdateGroup;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import top.ibase4j.core.base.BaseController;
////
import java.util.List;
////
/**
 * {{.Table.FunctionName}}Controller
 *
 * @author {{.Table.FunctionAuthor}}
 * @since {{.Table.CreateTime}}
*/
@RestController
@RequestMapping("/{{ .Table.ClassName |CaseCamelLower}}")
@Api(value = "{{.Table.FunctionName}}接口", tags = "{{.Table.FunctionName}}接口")
public class {{.Table.ClassName}}Controller extends BaseController<{{.Table.ClassName}}, I{{.Table.ClassName}}Service> {
////
       /**
       * POST {basePath}/{{ .Table.ClassName |CaseCamelLower}}/getPageList 分页查询数据
       *
       * @param page 分页查询对象 {@link {{.Table.ClassName}} 参数对象}
       * @return
       */
       @PostMapping("/getPageList")
       @ApiOperation(value = "分页查询数据")
       public QueryResultModel<List<{{.Table.ClassName}}VO>> getPageListEntity(
       @RequestBody Pagination<{{.Table.ClassName}}DTO> page) throws ServiceException {
           Page<{{.Table.ClassName}}VO> resultPage = this.getService().getPageList(page);
           return ServiceResultUtil.success(resultPage.getTotal(), resultPage.getRecords(), "查询成功");
       }
////

        /**
        * POST {basePath}/{{ .Table.ClassName |CaseCamelLower}}/getList 查询{{.Table.FunctionName}}列表
        *
        * @param param 参数对象 {@link {{.Table.ClassName}} 参数对象}
        * @return result
        */
        @PostMapping("/getList")
        @ApiOperation(value = "查询{{.Table.FunctionName}}列表")
        public QueryResultModel<List<{{.Table.ClassName}}VO>> getList(@RequestBody @Validated(QueryGroup.class) {{.Table.ClassName}} param) throws ServiceException {
            List<{{.Table.ClassName}}VO> list = this.getService().getList(param);
            Long total = (long) list.size();
            return ResultUtil.success(total, list, "查询成功");
        }
////

        /**
         * GET {basePath}/{{ .Table.ClassName |CaseCamelLower}}/{id} {{.Table.FunctionName}}详情
         *
         * @param id {{.Table.FunctionName}}id
         * @return
        */
        @GetMapping("/{id}")
        @ApiOperation(value = "{{.Table.FunctionName}}详情")
        public QueryResultModel<{{.Table.ClassName}}VO> get(@PathVariable("id") {{.Table.PkColumn.JavaType}} id) throws ServiceException {
            return this.getService().getById(id);
        }
////
        /**
         * POST {basePath}/{{ .Table.ClassName |CaseCamelLower}}/add 新增{{.Table.FunctionName}}记录
         *
         * @param entity 参数对象 {@link {{.Table.ClassName}} 参数对象}
         * @return result
        */
        @PostMapping("/add")
        @ApiOperation(value = "新增{{.Table.FunctionName}}记录")
        public ResultModel add(@RequestBody @Validated(AddGroup.class) {{.Table.ClassName}} entity) throws ServiceException {
            return this.getService().add(entity);
        }
////
        /**
         * POST {basePath}/{{ .Table.ClassName |CaseCamelLower}}/modify 更新{{.Table.FunctionName}}记录
         *
         * @param entity 参数对象 {@link {{.Table.ClassName}} 参数对象}
         * @return result
        */
        @PostMapping("/modify")
        @ApiOperation(value = "更新{{.Table.FunctionName}}记录")
        public ResultModel modify(@RequestBody @Validated(UpdateGroup.class) {{.Table.ClassName}} entity) throws ServiceException {
            return this.getService().modify(entity);
        }
////
        /**
         * POST {basePath}/{{ .Table.ClassName |CaseCamelLower}}/batchDisabled 批量禁用{{.Table.FunctionName}}记录
         *
         * @param ids 参数对象 ids
         * @return result
        */
        @PostMapping("/batchDisabled")
        @ApiOperation(value = "批量禁用{{.Table.FunctionName}}")
        public ResultModel batchDisabled(@RequestBody List<{{.Table.PkColumn.JavaType}}> ids) {
            return this.getService().batchDisabled(ids);
        }
////
        /**
         * POST {basePath}/{{ .Table.ClassName |CaseCamelLower}}/batchEnabled 批量启用{{.Table.FunctionName}}记录
         *
         * @param ids 参数对象 ids
         * @return result
        */
        @PostMapping("/batchEnabled")
        @ApiOperation(value = "批量启用{{.Table.FunctionName}}")
        public ResultModel batchEnabled(@RequestBody List<{{.Table.PkColumn.JavaType}}> ids) {
            return this.getService().batchEnabled(ids);
        }
////

}