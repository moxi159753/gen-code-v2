package {{.Table.PackageName}}.model.vo;
////
import {{.Table.PackageName}}.model.{{.Table.ClassName}};
////
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
////
import java.io.Serializable;
import java.util.List;
////
/**
* {{.Table.TableComment}} VO
*
* @author {{.Table.FunctionAuthor}}
* @since {{.Table.CreateTime}}
*/
@ApiModel(value = "{{.Table.TableComment}} VO")
@Data
@EqualsAndHashCode(callSuper = false)
public class {{.Table.ClassName}}VO extends {{.Table.ClassName}} implements Serializable {
////
{{if eq .Table.TplCategory "sub"}}
    /**
    *  注释:{{ .Table.SubTable.TableComment }}
    */
    private List<{{.Table.SubTable.ClassName}}> {{.Table.SubTable.ClassName | CaseCamelLower }}List;

{{end}}


}