package {{.Table.PackageName}}.model.dto;
////
import {{.Table.PackageName}}.model.{{.Table.ClassName}};
////
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
////
import java.io.Serializable;
import java.util.List;
////
/**
* {{.Table.TableComment}} DTO
*
* @author {{.Table.FunctionAuthor}}
* @since {{.Table.CreateTime}}
*/
@ApiModel(value = "{{.Table.TableComment}} DTO")
@Data
@EqualsAndHashCode(callSuper = false)
public class {{.Table.ClassName}}DTO extends {{.Table.ClassName}} implements Serializable {
////
{{if eq .Table.TplCategory "sub"}}
    /**
    *  注释:{{ .Table.SubTable.TableComment }}
    */
    private List<{{.Table.SubTable.ClassName}}> {{.Table.SubTable.ClassName | CaseCamelLower }}List;

{{end}}


}