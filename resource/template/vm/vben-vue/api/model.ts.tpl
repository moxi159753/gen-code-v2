import { BasicPageParams, BasicFetchResult } from '/@/api/model/baseModel';

/**
 * @vuese
 * @Description: 分页
 * @arg {*}
 */
export type paramsPageType = BasicPageParams & {
    {{range $index, $column := .Table.Columns}}
        {{if and (eq $column.IsQuery "true")}}
            {{$column.TsField}} : {{$column.TsType}}
        {{end}}
    {{end}}
};

/**
 * @vuese
 * @Description: 返回结果项
 * @arg {*}
 */
export interface resultItem {
    {{range $index, $column := .Table.Columns}}
        {{if and (eq $column.IsList "true")}}
            {{$column.TsField}} : {{$column.TsType}}
        {{end}}
    {{end}}
};

/**
 * @vuese
 * @Description: 新增项
 * @arg {*}
 */
export type addType = {
    {{range $index, $column := .Table.Columns}}
        {{if and (eq $column.IsInsert "true")}}
            {{$column.TsField}} : {{$column.TsType}}
        {{end}}
    {{end}}
};

/**
 * @vuese
 * @Description: 编辑项
 * @arg {*}
 */
export type editType = {
    {{range $index, $column := .Table.Columns}}
        {{if and (eq $column.IsEdit "true")}}
            {{$column.TsField}} : {{$column.TsType}}
        {{end}}
    {{end}}
};

export type IdsType = {
  ids: string[] | number[];
};


export type pageResultType = BasicFetchResult<resultItem>;