import { defHttp } from '/@/utils/http/axios';
import { useGlobSetting } from '/@/hooks/setting';
import { paramsPageType, pageResultType, IdsType, addType, editType} from './model'

const { genTableUrl = '' } = useGlobSetting();

{{$businessName := .Table.BusinessName | CaseCamelLower}}

{{$plugin:=""}}
{{if ContainsI $.Table.PackageName "plugins"}}
{{$plugin = "plugins/"}}
{{end}}


// 查询{{.Table.FunctionName}}列表
export const getAPI = (params:paramsPageType) =>defHttp.get<pageResultType>(
    {
        url: '/{{$plugin}}{{.Table.ModuleName}}/{{$businessName}}/list',
        params,
    },
    {
      apiUrl: genTableUrl,
      isReturnNativeResponse: true,
    },
  );

// 查询{{.Table.FunctionName}}详细
export const getDetailAPI = params =>defHttp.get<string>(
    {
        url: '/{{$plugin}}{{.Table.ModuleName}}/{{$businessName}}/get',
        params,
    },
    {
      apiUrl: genTableUrl,
      isReturnNativeResponse: true,
    },
  );

// 新增{{.Table.FunctionName}}
export const addAPI = (params:addType) =>defHttp.post<any>(
    {
        url: '/{{$plugin}}{{.Table.ModuleName}}/{{$businessName}}/add',
        params,
    },
    {
      apiUrl: genTableUrl,
      isReturnNativeResponse: true,
    },
  );

// 修改{{.Table.FunctionName}}
export const editAPI = (params:editType) =>defHttp.put<any>(
    {
        url: '/{{$plugin}}{{.Table.ModuleName}}/{{$businessName}}/edit',
        params,
    },
    {
        apiUrl: genTableUrl,
        isReturnNativeResponse: true,
    },
  );

// 删除{{.Table.FunctionName}}
export const delAPI = (params:IdsType) =>defHttp.delete<any>(
    {
        url: '/{{$plugin}}{{.Table.ModuleName}}/{{$businessName}}/delete',
        params,
    },
    {
        apiUrl: genTableUrl,
        isReturnNativeResponse: true,
    },
  );

{{$getUserList:=false}}

{{range $index,$column:= .Table.Columns}}
{{if and (HasSuffix $column.ColumnName "status") (eq $column.IsList "1") }}
// {{$.Table.FunctionName}}{{$column.ColumnComment}}修改
export function change{{$.Table.ClassName}}{{$column.GoField}}({{$.Table.PkColumn.HtmlField}},{{$column.HtmlField}}) {
  const data = {
    {{$.Table.PkColumn.HtmlField}},
    {{$column.HtmlField}}
  }
  return request({
    url: '/{{$plugin}}{{$.Table.ModuleName}}/{{$businessName}}/change{{$column.GoField}}',
    method: 'put',
    data:data
  })
}
{{end}}
{{if eq $column.JavaField "createdBy" "updatedBy"}}
{{$getUserList = true}}
{{end}}
{{end}}

{{if $getUserList}}
//获取用户信息列表
export function getUserList(uIds){
    return request({
     url: '/{{.Table.ModuleName}}/auth/usersGet',
     method: 'get',
     params: {ids:uIds}
   })
}
{{end}}