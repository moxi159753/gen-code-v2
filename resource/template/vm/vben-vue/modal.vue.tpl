<template>
  <BasicModal v-bind="$attrs" @register="registerModal" :title="getTitle" @ok="toSave">
    <BasicForm @register="registerForm" />
  </BasicModal>
</template>
<script lang="ts">
  import { defineComponent, ref, computed, unref } from 'vue';
  import { BasicModal, useModalInner } from '/@/components/Modal';
  import { BasicForm, useForm } from '/@/components/Form/index';
  import { formConfig } from "./config"
  import * as _API from './api';
  export default defineComponent({
    name: '{{.Table.ClassName}}Modal',
    components: { BasicModal, BasicForm },
    emits: ['success'],
    setup(props, { emit }) {
        const isUpdate = ref(false);
        const [registerForm, { resetFields, setFieldsValue, validate, setProps }] = useForm({
            schemas: formConfig(),
            showActionButtonGroup: false,
        });
        const [registerModal, { setModalProps, closeModal }] = useModalInner(async (data) => {
            await resetFields();
            setModalProps({confirmLoading: false});
            isUpdate.value = !!data?.isUpdate;
            if (unref(isUpdate)) {
                await setFieldsValue({...data.record});
            }
        });
        const getTitle = computed(() => (!unref(isUpdate) ? '新增{{.Table.FunctionName}}' : '编辑{{.Table.FunctionName}}'));

        const toSave = ()=>{
            try {
                const values = await validate();
                setModalProps({ confirmLoading: true });
                if (!isUpdate.value) {
                    await _API.addAPI(values);
                }else{
                    await _API.editAPI(values);
                }
                closeModal();
                emit('success');
            } finally {
                setModalProps({ confirmLoading: false });
            }
        }
        return { registerModal, registerForm, getTitle, toSave };
    }

  })
</script>