import { FormProps } from '/@/components/Table';
import { BasicColumn } from '/@/components/Table/src/types/table';

/**
 * @vuese
 * @author: 
 * @Description: 表格表单配置
 * @arg {*}
 * @since:
 */
export const tableFormConfig = ()=> {
    labelWidth: 100,
    schemas: [
    {{range $index, $column := .Table.Columns}}
        {{if and (eq $column.IsQuery "1")}}
            {field: '{{$column.JavaField}}',label: '{{$column.ColumnComment}}',component: '{{.CaseCamel ($column.HtmlType)}}',}
        {{end}}
    {{end}}
]as Partial<FormProps>


/**
 * @vuese
 * @author: 
 * @Description: 列表配置
 * @arg {*}
 * @since:
 */
export const column = ()=>[
    {{range $index, $column := .Table.Columns}}
        { 'title': '{{$column.ColumnComment}}', 'dataIndex': '{{$column.JavaField}}', 'width': 250 },
    {{end}}
] as BasicColumn[]


/**
 * @vuese
 * @author: 
 * @Description: 表单配置
 * @arg {*}
 * @since:
 */
export const formConfig = ()=>[
    {{range $index, $column := .Table.Columns}}
        { 'title': '{{$column.ColumnComment}}', 'dataIndex': '{{$column.JavaField}}', 'width': 250 },
    {{end}}
] as BasicColumn[]