<template>
    <div class="m-views-{{.Table.ClassName}}">
        <BasicTable @register="registerTable" :rowSelection="{ type: 'checkbox' }">
            <template #headerTop>
                <TableSelect :selectRows="getSelectRows()" @clear="clearSelectedRowKeys()" />
            </template>
            <template #toolbar>
                <a-button type="primary" @click="toAdd">
                    <Icon icon="ant-design:appstore-add-outlined"></Icon>
                    新增
                </a-button>
                <a-button type="primary" @click="toEdit" :disabled="getSelectRows().length < 1">
                    <Icon icon="ant-design:download-outlined"></Icon>
                    编辑
                </a-button>
                <a-button type="primary" @click="toDel(null, true)" :disabled="getSelectRows().length < 1">
                    <Icon icon="ant-design:delete-outlined"></Icon>
                    删除
                </a-button>
                <a-button type="primary" @click="toDetail">
                    <Icon icon="ant-design:import-outlined"></Icon>
                    详情
                </a-button>
            </template>
        </BasicTable>
        <{{.Table.ClassName}}-modal @register="registerModal" @success="reload" />
    </div>
</template>
<script lang="ts">
    import { BasicTable, TableAction, useTable } from '/@/components/Table';
    import { useModal } from '/@/components/Modal';
    import { column , tableFormConfig } from "./config"
    import * as _API from "./api"
    import CRUD from "@/crud/init"
    import TableSelect from '/@/components/Custom/TableSelect/index.vue';
    import {{.Table.ClassName}}Modal from './modal.vue';
    export default defineComponent({
        name: '{{.Table.ClassName}}',
        components: { BasicTable, TableSelect, {{.Table.ClassName}}Modal},
        setup(){
            const crud = CRUD({
                title:"{{.Table.FunctionName}}",
                crudMethod: { ..._API },
            })
            const [registerTable, { reload, getSelectRowKeys, getSelectRows, clearSelectedRowKeys }] = useTable({
                api: _API.getAPI,
                columns: column(),
                formConfig: tableFormConfig(),
                useSearchForm: true,
                showTableSetting: true,
                size: 'small',
                bordered: true,
                showIndexColumn: true,
                canResize: false,
                clickToRowSelect: false,
                rowKey: 'id',
            });
            const toAdd = () =>{
                openModal(true, {isUpdate: false});
            }
            const toEdit = (record: Recordable) =>{
                openModal(true, {isUpdate: true,record});
            }
            const toDel = (record: Recordable, useConfirm: Boolean)=> {
                crud.toDel({
                    params: {
                        ids: (record && [record.id]) ?? getSelectRowKeys(),
                    },
                    useConfirm,
                    refresh: false,
                })
                .then((res) => reload());
            }
            const toDetail = (record: Recordable) =>{
                crud.toDetail({params:{id:record.id}})
            }
            return {
                registerTable,
                getSelectRows
                clearSelectedRowKeys,
                toAdd,
                toEdit,
                toDel,
                toDetail
            }
        }

    })
</script>
<style lang="less>
.m-views-{{.Table.ClassName}}{

}
</style>