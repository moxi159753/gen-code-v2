<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE mapper PUBLIC "-//mybatis.org//DTD Mapper 3.0//EN" "http://mybatis.org/dtd/mybatis-3-mapper.dtd">
<mapper namespace="{{.Table.PackageName}}.mapper.{{.Table.ClassName}}Mapper">

    <resultMap id="BaseResultMap" type="com.moxi.mogublog.commons.entity..{{.Table.ClassName}}">
{{range $index, $column := .Table.Columns}}
        <result property="{{$column.JavaField}}"    column="{{$column.ColumnName}}"/>
{{end}}
    </resultMap>

{{ $length := len .Table.Columns }}
    <sql id="Base_Column_List">
        {{range $index, $column := .Table.Columns}} `{{$column.ColumnName}}`{{if ne (Sum $index 1) $length}},{{end}}{{end}}
    </sql>


</mapper>