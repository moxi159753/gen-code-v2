package {{.Table.PackageName}}.{{.Table.ModuleName}}.mapper;

import com.moxi.mogublog.commons.entity.{{.Table.ClassName}};
import com.moxi.mougblog.base.mapper.SuperMapper;

////
/**
 * {{.Table.FunctionName}}Mapper接口
 *
 * @author {{.Table.FunctionAuthor}}
 * @since {{.Table.CreateTime}}
 */
public interface {{.Table.ClassName}}Mapper extends SuperMapper<{{.Table.ClassName}}> {

}