package com.moxi.mogublog.commons.entity.entity;
////


import com.baomidou.mybatisplus.annotation.*;
import com.moxi.mougblog.base.entity.SuperEntity;
import lombok.Data;
import java.util.List;

{{range $index, $import := .ImportList}}
import {{$import}};
{{end}}


////
/**
* {{.Table.FunctionName}}对象 {{.Table.TableName}}
*
* @author {{.Table.FunctionAuthor}}
* @since {{.Table.CreateTime}}
*/
@Data
@TableName("{{.Table.TableName}}")
public class {{.Table.ClassName}} extends SuperEntity<{{.Table.ClassName}}>{
////
    private static final long serialVersionUID = 1L;


{{range $index, $column := .Table.Columns}}
////
  {{if not  (InArray PublicFields $column.ColumnName)}}
    /**
    *  Java类型:{{ $column.JavaType }}
    *  数据库类型:{{ $column.ColumnType }}
    *  注释:{{ $column.ColumnComment }}
    */
    {{if eq $column.JavaType "Long"}}
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    {{end}}
    @TableField(value = "{{$column.ColumnName}}")
    {{if eq $column.IsPk "1"}}
    @TableId(value = "{{$column.ColumnName}}")
    {{end}}
    private {{$column.JavaType}} {{$column.JavaField}};


  {{end}}
{{end}}


{{range $index, $column := .Table.Columns}}
  {{if not  (InArray PublicFields $column.ColumnName)}}
    /**
     * {{ $column.ColumnComment }}
    */
    public static final String COL_{{$column.ColumnName| SnakeScreamingCase}} = "{{$column.ColumnName}}";


  {{end}}
{{end}}

}