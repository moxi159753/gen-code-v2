package {{.Table.PackageName}}.{{.Table.ModuleName}}.service;
////
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.moxi.mogublog.commons.entity.{{.Table.ClassName}};
import com.moxi.mogublog.{{.Table.ModuleName}}.vo.{{.Table.ClassName}}VO;
import com.moxi.mougblog.base.service.SuperService;
////
import java.util.List;
import java.util.Map;
////

/**
 * {{.Table.FunctionName}}Service接口
 *
 * @author {{.Table.FunctionAuthor}}
 * @since {{.Table.CreateTime}}
 */
public interface {{.Table.ClassName}}Service extends SuperService<{{.Table.ClassName}}>{
////
     /**
      * 分页查询
      *
      * @param pagination 查询条件
      * @return
     */
    public IPage<{{.Table.ClassName}}VO> getPageList({{.Table.ClassName}}VO param);
////

    /**
    * 查询{{.Table.FunctionName}}列表
    *
    * @param {{ .Table.ClassName |CaseCamelLower}} {{.Table.FunctionName}}
    * @return {{.Table.FunctionName}}集合
    */
    List<{{.Table.ClassName}}VO> getList({{.Table.ClassName}} {{ .Table.ClassName |CaseCamelLower}});
////

     /**
      * 查询{{.Table.FunctionName}}详情
      *
      * @param id id
      * @return {{.Table.FunctionName}}
     */
     {{.Table.ClassName}}VO getById({{.Table.PkColumn.JavaType}} id);
////
    /**
     * 添加{{.Table.FunctionName}}
     * @param model {{.Table.FunctionName}}信息
     * @return result
     * @throws ServiceException
    */
    String add({{.Table.ClassName}}VO model);
////
    /**
     * 修改{{.Table.FunctionName}}
     * @param model {{.Table.FunctionName}}信息
     * @return result
     * @throws ServiceException
    */
    String edit({{.Table.ClassName}} model);
////
    /**
     * 删除{{.Table.FunctionName}}记录
     * @param id  {{.Table.FunctionName}}ID
     * @return result
    */
    ServiceResultModel delete({{.Table.PkColumn.JavaType}} id);
////
    /**
     * 批量删除{{.Table.FunctionName}}记录
     *
     * @param ids 需要删除的{{.Table.FunctionName}}主键集合
     * @return result
     */
     ServiceResultModel batchDelete(List<{{.Table.PkColumn.JavaType}}> ids);

}