package v1
////
import (
	"{{.Table.SystemName}}/internal/model/{{.Table.ModuleName}}"
	"{{.Table.SystemName}}/internal/model/{{.Table.ModuleName}}/entity"
	"github.com/gogf/gf/v2/frame/g"
)
////
// {{.Table.ClassName}}PageListReq 分页查询{{.Table.FunctionName}}Req
type {{.Table.ClassName}}PageListReq struct {
	g.Meta `path:"/pageList" tags:"{{.Table.ClassName}}" method:"post" summary:"分页查询{{.Table.FunctionName}}"`
	{{.Table.ModuleName}}.{{.Table.ClassName}}
}
////
// {{.Table.ClassName}}PageListRes 分页查询{{.Table.FunctionName}}Res
type {{.Table.ClassName}}PageListRes struct {
	Total int                   `json:"total" dc:"行数"`
	Rows  []*entity.{{.Table.ClassName}} `json:"rows" dc:"{{.Table.FunctionName}}数组"`
}
////
// {{.Table.ClassName}}ListReq 列表查询{{.Table.FunctionName}}Req
type {{.Table.ClassName}}ListReq struct {
	g.Meta `path:"/list" tags:"{{.Table.ClassName}}" method:"post" summary:"列表查询{{.Table.FunctionName}}"`
	entity.{{.Table.ClassName}}
}
////
// {{.Table.ClassName}}ListRes 列表查询Res
type {{.Table.ClassName}}ListRes struct {
	Rows  []*entity.{{.Table.ClassName}} `json:"rows" dc:"{{.Table.FunctionName}}数组"`
}
////
// {{.Table.ClassName}}GetReq 查询{{.Table.FunctionName}}详情Req
type {{.Table.ClassName}}GetReq struct {
	g.Meta `path:"/:id" tags:"{{.Table.ClassName}}" method:"get" summary:"列表查询{{.Table.FunctionName}}"`
	{{.Table.PkColumn.GoField}}   {{.Table.PkColumn.GoType}} `json:"{{.Table.PkColumn.JavaField}}" dc:"{{ .Table.PkColumn.ColumnComment }}"`
}
////
// {{.Table.ClassName}}GetRes 查询{{.Table.FunctionName}}详情Res
type {{.Table.ClassName}}GetRes struct {
	*entity.{{.Table.ClassName}}
}
////
// {{.Table.ClassName}}AddReq 添加{{.Table.FunctionName}}Req
type {{.Table.ClassName}}AddReq struct {
	g.Meta `path:"/" tags:"{{.Table.ClassName}}" method:"post" summary:"添加{{.Table.FunctionName}}"`
{{range $index, $column := .Table.Columns}}
    {{if $column.IsInsert }}
    {{$column.GoField}}  {{$column.GoType}} `json:"{{$column.JavaField}}" {{if $column.IsRequired}}v:"required#{{ $column.ColumnComment }}不能为空"{{end}}  dc:"{{ $column.ColumnComment }}"` //{{ $column.ColumnComment }}
    {{end}}
{{end}}
}
////
// {{.Table.ClassName}}AddRes 添加{{.Table.FunctionName}}Req
type {{.Table.ClassName}}AddRes struct {
	Msg  string  `json:"msg" dc:"添加提示"`
}
////
// {{.Table.ClassName}}EditReq 编辑{{.Table.FunctionName}}Req
type {{.Table.ClassName}}EditReq struct {
	g.Meta `path:"/" tags:"{{.Table.ClassName}}" method:"put" summary:"编辑{{.Table.FunctionName}}"`
	{{.Table.PkColumn.GoField}} {{.Table.PkColumn.GoType}} `json:"{{.Table.PkColumn.JavaField}}" v:"required#{{ .Table.PkColumn.ColumnComment }}不能为空"  dc:"{{ .Table.PkColumn.ColumnComment }}"` //主键
	VersionNumber string `json:"versionNumber" v:"required#未识别到版本号" dc:"版本号"` //版本号
{{range $index, $column := .Table.Columns}}
    {{if $column.IsEdit }}
    {{$column.GoField}}  {{$column.GoType}} `json:"{{$column.JavaField}}" {{if $column.IsRequired}}v:"required#{{ $column.ColumnComment }}不能为空"{{end}}  dc:"{{ $column.ColumnComment }}"` //{{ $column.ColumnComment }}
    {{end}}
{{end}}
}
////
// {{.Table.ClassName}}EditRes 编辑{{.Table.FunctionName}}Res
type {{.Table.ClassName}}EditRes struct {
	Msg  string  `json:"msg" dc:"编辑提示"`
}
////
// {{.Table.ClassName}}EditStateReq 编辑{{.Table.FunctionName}}状态Req
type {{.Table.ClassName}}EditStateReq struct {
	g.Meta `path:"/state" tags:"{{.Table.ClassName}}" method:"put" summary:"批量编辑{{.Table.FunctionName}}状态"`
	Ids    []{{.Table.PkColumn.GoType}} `json:"ids" v:"required#字典主键不能为空"  dc:"id集合"`   //主键
	State  int8    `json:"state" v:"required#字典状态不能为空"  dc:"字典状态"` //状态
}
////
// {{.Table.ClassName}}DelReq 删除{{.Table.FunctionName}}Req
type {{.Table.ClassName}}DelReq struct {
	g.Meta `path:"/" tags:"{{.Table.ClassName}}" method:"delete" summary:"删除{{.Table.FunctionName}}"`
	Ids    []{{.Table.PkColumn.GoType}} `json:"ids" v:"required#请选择需要删除的数据" dc:"id集合"`
}
////
// {{.Table.ClassName}}DelRes 删除{{.Table.FunctionName}}Res
type {{.Table.ClassName}}DelRes struct {
	Msg  string  `json:"msg" dc:"删除提示"`
}