package {{.Table.ModuleName}}
////
import (
	"{{.Table.SystemName}}/internal/controller/{{.Table.ModuleName}}"
	"github.com/gogf/gf/v2/net/ghttp"
)
////
func Init{{.Table.ClassName}}(group *ghttp.RouterGroup) {
	group.Group("/{{ .Table.ClassName |CaseCamelLower}}", func(group *ghttp.RouterGroup) {
    		group.Bind({{.Table.ModuleName}}.{{.Table.ClassName}})
    })
}
