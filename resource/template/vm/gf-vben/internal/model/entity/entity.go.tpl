package entity
////
import (
    "github.com/gogf/gf/v2/os/gtime"
)
////
type {{.Table.ClassName}} struct {
{{range $index, $column := .Table.Columns}}
    {{$column.GoField}}  {{$column.GoType}} `json:"{{$column.JavaField}}" {{if $column.IsQuery}}q:"{{$column.QueryType}}"{{else}}q:"-"{{end}} dc:"{{ $column.ColumnComment }}"` //{{ $column.ColumnComment }}
{{end}}
}
