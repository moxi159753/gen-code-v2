package {{.Table.ModuleName}}
////
import (
	"gf-vben-admin/internal/model"
	"gf-vben-admin/internal/model/{{.Table.ModuleName}}/entity"
)
////
type {{.Table.ClassName}} struct {
    model.PageReq
	entity.{{.Table.ClassName}}
}
