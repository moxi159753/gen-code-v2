package {{.Table.ModuleName}}
////
import (
	"context"
	v1 "{{.Table.SystemName}}/api/{{.Table.ModuleName}}/v1"
	model "{{.Table.SystemName}}/internal/model/{{.Table.ModuleName}}"
	service "{{.Table.SystemName}}/internal/service/{{.Table.ModuleName}}"
	"github.com/gogf/gf/v2/util/gconv"
)
////
var (
	{{.Table.ClassName}} = c{{.Table.ClassName}}{}
)
type c{{.Table.ClassName}} struct{}
////
// PageList 分页查询{{.Table.FunctionName}}
func (c *c{{.Table.ClassName}}) PageList(ctx context.Context, req *v1.{{.Table.ClassName}}PageListReq) (res *v1.{{.Table.ClassName}}PageListRes, err error) {
	total, list, err := service.{{.Table.ClassName}}().PageList(ctx, req.{{.Table.ClassName}})
	if err != nil {
		return nil, err
	}
	res = &v1.{{.Table.ClassName}}PageListRes{
		Total: total,
		Rows:  list,
	}
	return
}
////
// List 列表查询{{.Table.FunctionName}}
func (c *c{{.Table.ClassName}}) List(ctx context.Context, req *v1.{{.Table.ClassName}}ListReq) (res *v1.{{.Table.ClassName}}ListRes, err error) {
	list, err := service.{{.Table.ClassName}}().List(ctx, req.{{.Table.ClassName}})
	if err != nil {
		return nil, err
	}
	res = &v1.{{.Table.ClassName}}ListRes{
		Rows: list,
	}
	return
}
////
// Get 查询{{.Table.FunctionName}}详情
func (c *c{{.Table.ClassName}}) Get(ctx context.Context, req *v1.{{.Table.ClassName}}GetReq) (res *v1.{{.Table.ClassName}}GetRes, err error) {
	entity, err := service.{{.Table.ClassName}}().Get(ctx, req.{{.Table.PkColumn.GoField}})
	if err != nil {
		return nil, err
	}
	res = &v1.{{.Table.ClassName}}GetRes{
		{{.Table.ClassName}}: entity,
	}
	return
}
////
// Add 添加{{.Table.FunctionName}}
func (c *c{{.Table.ClassName}}) Add(ctx context.Context, req *v1.{{.Table.ClassName}}AddReq) (res *v1.{{.Table.ClassName}}AddRes, err error) {
	in := new(model.{{.Table.ClassName}})
	err = gconv.Struct(req, in)
	if err != nil {
		return nil, err
	}
	err = service.{{.Table.ClassName}}().Add(ctx, *in)
	return
}
////
// Edit 编辑{{.Table.FunctionName}}
func (c *c{{.Table.ClassName}}) Edit(ctx context.Context, req *v1.{{.Table.ClassName}}EditReq) (res *v1.{{.Table.ClassName}}EditRes, err error) {
	in := new(model.{{.Table.ClassName}})
	err = gconv.Struct(req, in)
	if err != nil {
		return nil, err
	}
	err = service.{{.Table.ClassName}}().Edit(ctx, *in)
	return
}
////
// EditState 编辑{{.Table.FunctionName}}状态
func (c *c{{.Table.ClassName}}) EditState(ctx context.Context, req *v1.{{.Table.ClassName}}EditStateReq) (res *v1.{{.Table.ClassName}}EditRes, err error) {
	err = service.{{.Table.ClassName}}().EditState(ctx, req.Ids, req.State)
	return
}

////
// Delete 删除{{.Table.FunctionName}}
func (c *c{{.Table.ClassName}}) Delete(ctx context.Context, req *v1.{{.Table.ClassName}}DelReq) (res *v1.{{.Table.ClassName}}DelRes, err error) {
	err = service.{{.Table.ClassName}}().Delete(ctx, req.Ids)
	return
}
