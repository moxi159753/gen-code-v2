package internal
////
import (
	"context"
	"github.com/gogf/gf/v2/database/gdb"
	"github.com/gogf/gf/v2/frame/g"
)
////
type {{.Table.ClassName}}Dao struct {
	table   string                   // 表名称
	group   string                   // 数据源分组，默认default
	columns {{.Table.ClassName}}Columns // 表字段
	columnMap map[string]string //表字段map
}
////
// {{.Table.ClassName}}Columns defines and stores column names for table gen_database.
type {{.Table.ClassName}}Columns struct {
{{range $index, $column := .Table.Columns}}
    {{$column.GoField}}  string //{{ $column.ColumnComment }}
{{end}}
}
////
//  {{ .Table.ClassName |CaseCamelLower}}Columns holds the columns for table mes_gf_mst_yield_line.
var {{ .Table.ClassName |CaseCamelLower}}Columns = {{.Table.ClassName}}Columns{
{{range $index, $column := .Table.Columns}}
    {{$column.GoField}}:  "{{$column.ColumnName}}", //{{ $column.ColumnComment }}
{{end}}
}

var {{ .Table.ClassName |CaseCamelLower}}ColumnMap = map[string]string{
{{range $index, $column := .Table.Columns}}
    "{{$column.GoField}}":  "{{$column.ColumnName}}", //{{ $column.ColumnComment }}
{{end}}
}

// New{{.Table.ClassName}}Dao creates and returns a new DAO object for table data access.
func New{{.Table.ClassName}}Dao() *{{.Table.ClassName}}Dao {
	return &{{.Table.ClassName}}Dao{
		group:   "default",
		table:   "{{.Table.TableName}}",
		columns: {{ .Table.ClassName |CaseCamelLower}}Columns,
        columnMap: {{ .Table.ClassName |CaseCamelLower}}ColumnMap,
	}
}

// DB retrieves and returns the underlying raw database management object of current DAO.
func (dao *{{.Table.ClassName}}Dao) DB() gdb.DB {
	return g.DB(dao.group)
}

// Table returns the table name of current dao.
func (dao *{{.Table.ClassName}}Dao) Table() string {
	return dao.table
}

// Columns returns all column names of current dao.
func (dao *{{.Table.ClassName}}Dao) Columns() {{.Table.ClassName}}Columns {
	return dao.columns
}

// ColumnMap returns all column map of current dao.
func (dao *{{.Table.ClassName}}Dao) ColumnMap() map[string]string {
	return dao.columnMap
}


// Group returns the configuration group name of database of current dao.
func (dao *{{.Table.ClassName}}Dao) Group() string {
	return dao.group
}

// Ctx creates and returns the Model for current DAO, It automatically sets the context for current operation.
func (dao *{{.Table.ClassName}}Dao) Ctx(ctx context.Context) *gdb.Model {
	return dao.DB().Model(dao.table).Safe().Ctx(ctx)
}

// Transaction wraps the transaction logic using function f.
// It rollbacks the transaction and returns the error from function f if it returns non-nil error.
// It commits the transaction and returns nil if function f returns nil.
//
// Note that, you should not Commit or Rollback the transaction in function f
// as it is automatically handled by this function.
func (dao *{{.Table.ClassName}}Dao) Transaction(ctx context.Context, f func(ctx context.Context, tx *gdb.TX) error) (err error) {
	return dao.Ctx(ctx).Transaction(ctx, f)
}
