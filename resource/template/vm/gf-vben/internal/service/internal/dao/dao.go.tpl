package dao
////
import (
	"{{.Table.SystemName}}/internal/service/{{.Table.ModuleName}}/internal/dao/internal"
)
////
type {{ .Table.ClassName |CaseCamelLower}}Dao struct {
	*internal.{{.Table.ClassName}}Dao
}
////
var (
	{{.Table.ClassName}} = {{ .Table.ClassName |CaseCamelLower}}Dao{
		internal.New{{.Table.ClassName}}Dao(),
	}
)
