package do

import (
	"github.com/gogf/gf/v2/frame/g"
	"github.com/gogf/gf/v2/os/gtime"
)

type {{.Table.ClassName}} struct {
    g.Meta         `orm:"table:{{.Table.TableName}}, do:true"`
{{range $index, $column := .Table.Columns}}
     {{if eq $column.GoType "*gtime.Time"}}
     {{$column.GoField}}  *gtime.Time //{{ $column.ColumnComment }}
     {{else}}
     {{$column.GoField}}  interface{} //{{ $column.ColumnComment }}
     {{end}}
{{end}}
}
