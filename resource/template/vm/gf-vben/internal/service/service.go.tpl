package {{.Table.ModuleName}}
////
import (
	"context"
	"{{.Table.SystemName}}/consts"
	model "{{.Table.SystemName}}/internal/model/{{.Table.ModuleName}}"
	"{{.Table.SystemName}}/internal/model/{{.Table.ModuleName}}/entity"
	"{{.Table.SystemName}}/internal/service/{{.Table.ModuleName}}/internal/dao"
	utils "gf-vben-admin/utility"
	"github.com/gogf/gf/v2/database/gdb"
	"github.com/gogf/gf/v2/errors/gerror"
	"github.com/gogf/gf/v2/frame/g"
)
////
type (
	s{{.Table.ClassName}} struct{}
)
////
var (
	ins{{.Table.ClassName}} = s{{.Table.ClassName}}{}
)
////
func {{.Table.ClassName}}() *s{{.Table.ClassName}} {
	return &ins{{.Table.ClassName}}
}
////
//PageList 分页查询{{.Table.FunctionName}}
func (s *s{{.Table.ClassName}}) PageList(ctx context.Context, param model.{{.Table.ClassName}}) (total int, result []*entity.{{.Table.ClassName}}, err error) {
    result = make([]*entity.{{.Table.ClassName}}, 0)
	daoModel := dao.{{.Table.ClassName}}.Ctx(ctx)
	columnMap := dao.{{.Table.ClassName}}.ColumnMap()
	daoModel, err = utils.GetWrapper(param, daoModel, columnMap)
	if err != nil {
		return
	}
	total, err = daoModel.Count()
	if err != nil {
		g.Log().Error(ctx, err)
		err = gerror.New("获取总行数失败")
		return
	}
	if total == 0 {
		return
	}
	err = daoModel.Page(param.PageNum, param.PageSize).Scan(&result)
	if err != nil {
		g.Log().Error(ctx, err)
		err = gerror.New("获取数据失败")
	}
	return
}
////
// List 列表查询{{.Table.FunctionName}}
func (s *s{{.Table.ClassName}}) List(ctx context.Context, param entity.{{.Table.ClassName}}) (result []*entity.{{.Table.ClassName}}, err error) {
    result = make([]*entity.{{.Table.ClassName}}, 0)
	daoModel := dao.{{.Table.ClassName}}.Ctx(ctx)
	columnMap := dao.{{.Table.ClassName}}.ColumnMap()
	daoModel, err = utils.GetWrapper(param, daoModel, columnMap)
	if err != nil {
		return
	}
	err = daoModel.Scan(&result)
	if err != nil {
		g.Log().Error(ctx, err)
		err = gerror.New("获取数据失败")
	}
	return
}
////
// Get 查询{{.Table.FunctionName}}详情
func (s *s{{.Table.ClassName}}) Get(ctx context.Context, {{.Table.PkColumn.JavaField}} {{.Table.PkColumn.GoType}}) (result *entity.{{.Table.ClassName}}, err error) {
    result = new(entity.{{.Table.ClassName}})
	daoModel := dao.{{.Table.ClassName}}.Ctx(ctx)
	err = daoModel.Where(dao.{{.Table.ClassName}}.Columns().{{.Table.PkColumn.GoField}},{{.Table.PkColumn.JavaField}}).Scan(&result)
	if err != nil {
		g.Log().Error(ctx, err)
		err = gerror.New("获取数据失败")
	}
	return
}
////
// Add 添加{{.Table.FunctionName}}
func (s *s{{.Table.ClassName}}) Add(ctx context.Context, in model.{{.Table.ClassName}}) (err error) {
	err = dao.{{.Table.ClassName}}.Ctx(ctx).Transaction(ctx, func(ctx context.Context, tx *gdb.TX) error {
		_, err := tx.Model(dao.{{.Table.ClassName}}.Table()).Ctx(ctx).OmitEmpty().Data(in).Insert()
		return err
	})
	return
}
////
// Edit 编辑{{.Table.FunctionName}}
func (s *s{{.Table.ClassName}}) Edit(ctx context.Context, in model.{{.Table.ClassName}}) (err error) {
	err = dao.{{.Table.ClassName}}.Ctx(ctx).Transaction(ctx, func(ctx context.Context, tx *gdb.TX) error {
		_, err := tx.Model(dao.{{.Table.ClassName}}.Table()).Ctx(ctx).OmitEmpty().Data(in).Where(g.Map{
			dao.{{.Table.ClassName}}.Columns().{{.Table.PkColumn.GoField}}: in.{{.Table.PkColumn.GoField}},
			consts.VersionNumberColumn:       in.VersionNumber,
		}).Update()
		return err
	})
	return
}
////
// EditState 编辑{{.Table.FunctionName}}状态
func (s *s{{.Table.ClassName}}) EditState(ctx context.Context, ids []{{.Table.PkColumn.GoType}}, state int8) (err error) {
	err = dao.{{.Table.ClassName}}.Ctx(ctx).Transaction(ctx, func(ctx context.Context, tx *gdb.TX) error {
		_, err := tx.Model(dao.{{.Table.ClassName}}.Table()).Ctx(ctx).Data(g.Map{consts.StateColumn: state}).Where(g.Map{
			dao.{{.Table.ClassName}}.Columns().{{.Table.PkColumn.GoField}}: ids,
		}).Update()
		return err
	})
	return
}
////
// Delete 删除{{.Table.FunctionName}}
func (s *s{{.Table.ClassName}}) Delete(ctx context.Context, ids []{{.Table.PkColumn.GoType}}) (err error) {
	err = dao.{{.Table.ClassName}}.Ctx(ctx).Transaction(ctx, func(ctx context.Context, tx *gdb.TX) error {
		_, err := tx.Model(dao.{{.Table.ClassName}}.Table()).Ctx(ctx).Where(g.Map{
			dao.{{.Table.ClassName}}.Columns().{{.Table.PkColumn.GoField}}: ids,
		}).Delete()
		return err
	})
	return
}


