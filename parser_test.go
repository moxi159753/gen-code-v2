package main

import (
	"context"
	"fmt"
	"gen-code-v2/consts"
	"github.com/blastrain/vitess-sqlparser/tidbparser/ast"
	"github.com/blastrain/vitess-sqlparser/tidbparser/parser"
	"github.com/gogf/gf/v2/frame/g"
	"github.com/gogf/gf/v2/os/gfile"
	"github.com/gogf/gf/v2/os/grpool"
	"github.com/gogf/gf/v2/text/gstr"
	"sync"
	"testing"
	"time"
)

func TestCreateTableWithPartition(t *testing.T) {
	sql := `create table base_sys_menu
(
    id         int auto_increment comment 'ID'
        primary key,
    createTime datetime(6) default CURRENT_TIMESTAMP(6) not null comment '创建时间',
    updateTime datetime(6) default CURRENT_TIMESTAMP(6) not null on update CURRENT_TIMESTAMP(6) comment '更新时间',
    parentId   bigint                                   null comment '父菜单ID',
    name       varchar(255)                             not null comment '菜单名称',
    router     varchar(255)                             null comment '菜单地址',
    perms      text                                     null comment '权限标识',
    type       tinyint     default 0                    not null comment '类型 0：目录 1：菜单 2：按钮',
    icon       varchar(255)                             null comment '图标',
    orderNum   int         default 0                    not null comment '排序',
    viewPath   varchar(255)                             null comment '视图地址',
    keepAlive  tinyint     default 1                    not null comment '路由缓存',
    isShow     tinyint     default 1                    not null comment '是否显示#0:不显示,1:显示'
)
    comment 'admin菜单' charset = utf8mb4;
create table gen_table_column
(
    column_id      bigint auto_increment comment '编号'
        primary key,
    table_id       varchar(64)               null comment '归属表编号',
    column_name    varchar(200)              null comment '列名称',
    column_comment varchar(500)              null comment '列描述',
    column_type    varchar(100)              null comment '列类型',
    java_type      varchar(500)              null comment 'JAVA类型',
    java_field     varchar(200)              null comment 'JAVA字段名',
    go_type        varchar(500)              null comment 'GO类型',
    go_field       varchar(200)              null comment 'GO字段名',
    is_pk          char                      null comment '是否主键（1是）',
    is_increment   char                      null comment '是否自增（1是）',
    is_required    char                      null comment '是否必填（1是）',
    is_insert      char                      null comment '是否为插入字段（1是）',
    is_edit        char                      null comment '是否编辑字段（1是）',
    is_list        char                      null comment '是否列表字段（1是）',
    is_query       char                      null comment '是否查询字段（1是）',
    query_type     varchar(200) default 'EQ' null comment '查询方式（等于、不等于、大于、小于、范围）',
    html_type      varchar(200)              null comment '显示类型（文本框、文本域、下拉框、复选框、单选框、日期控件）',
    dict_type      varchar(200) default ''   null comment '字典类型',
    sort           int                       null comment '排序',
    create_by      varchar(64)  default ''   null comment '创建者',
    create_time    datetime                  null comment '创建时间',
    update_by      varchar(64)  default ''   null comment '更新者',
    update_time    datetime                  null comment '更新时间'
)
    comment '代码生成业务表字段';
`
	var createSqlArray = make([]string, 0)
	parse, err := parser.New().Parse(sql, "", "")
	if err != nil {
		return
	}
	for _, stmtNode := range parse {
		switch stmtNode := stmtNode.(type) {
		case *ast.CreateTableStmt:
			createSqlArray = append(createSqlArray, stmtNode.Text())
		}
	}

}

func TestFile(t *testing.T) {
	path := "resource/template/vm/java_test"
	fmt.Println(gfile.Dir(path))
}

type Count struct {
	sync.Mutex
	Count int
}

func TestHttpA(t *testing.T) {
	client := g.Client()
	//client.SetProxy("http://127.0.0.1:7890")
	client.SetTimeout(10 * time.Second)
	client.HeaderRaw(`Connection: keep-alive
Cache-Control: max-age=0
Upgrade-Insecure-Requests: 1
User-Agent: MQQBrowser/26 Mozilla/5.0 (Linux; U; Android 2.3.7; zh-cn; MB200 Build/GRJ22; CyanogenMod-7) AppleWebKit/533.1 (KHTML, like Gecko) Version/4.0 Mobile Safari/533.1
`)
	pool := grpool.New(10)
	defer pool.Close()
	wg := sync.WaitGroup{}

	for i := 0; i < 10; i++ {
		wg.Add(1)
		pool.Add(context.TODO(), func(ctx context.Context) {
			body := client.GetContent(context.TODO(), "http://t.cn/A66WwyMG")
			fmt.Println(body)
			wg.Done()

		})
	}
	wg.Wait()

}

type logClosure func(format string, v ...interface{})

func LoggerWrapper(logType string) logClosure {
	return func(format string, v ...interface{}) {
		fmt.Printf(fmt.Sprintf("[%s] %s", logType, format), v...)
		fmt.Println() // 换行
	}
}

func TestLogger(t *testing.T) {
	infoLogger := LoggerWrapper("INFO")
	warningLogger := LoggerWrapper("WARNING")
	infoLogger("this is a %s log", "info")
	warningLogger("this is a %s log", "warning")
}

var once sync.Once

type manager struct{ name string }

var single *manager

func Singleton() *manager {
	once.Do(func() {
		single = &manager{"a"}
	})
	return single
}

func TestSing(t *testing.T) {
	singleton := Singleton()
	singleton.name = "b"
	fmt.Println(singleton)
	singleton1 := Singleton()
	fmt.Println(singleton1)
}

func TestUrl(t *testing.T) {
	url := "/auto/code/test/getById?id=1"
	url = gstr.Replace(url, consts.AutoCode, "") // test/getById?id=1
	if index := gstr.PosI(url, consts.Slash); index > -1 {
		url = gstr.TrimLeft(url, consts.Slash)
	}
	module := gstr.StrTillEx(url, consts.Slash)
	fmt.Println(module)

}
