package router

import (
	"gen-code-v2/internal/controller"
	"github.com/gogf/gf/v2/net/ghttp"
)

func InitTpl(group *ghttp.RouterGroup) {
	group.Group("/tpl", func(group *ghttp.RouterGroup) {
		group.Bind(controller.Template)
	})
}
