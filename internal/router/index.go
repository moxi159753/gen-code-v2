package router

import (
	"gen-code-v2/internal/controller"
	"github.com/gogf/gf/v2/net/ghttp"
)

func InitIndex(group *ghttp.RouterGroup) {
	group.Bind(controller.Index)

}
