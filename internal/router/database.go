package router

import (
	"gen-code-v2/internal/controller"
	"github.com/gogf/gf/v2/net/ghttp"
)

func InitDB(group *ghttp.RouterGroup) {
	group.Group("/db", func(group *ghttp.RouterGroup) {
		group.Bind(controller.Database)
	})
}
