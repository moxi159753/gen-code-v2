package router

import (
	"gen-code-v2/internal/controller"
	"github.com/gogf/gf/v2/net/ghttp"
)

func InitTable(group *ghttp.RouterGroup) {
	group.Group("/gen", func(group *ghttp.RouterGroup) {
		group.Bind(controller.Table)
	})
}
