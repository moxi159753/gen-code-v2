package middle

import (
	"github.com/gogf/gf/v2/frame/g"
	"github.com/gogf/gf/v2/net/ghttp"
	"net/http"
)

type BaseResponse struct {
	Code    int         `json:"code" dc:"code"`
	Type    string      `json:"type" dc:"type"`
	Message string      `json:"message" dc:"message"`
	Result  interface{} `json:"result" dc:"result"`
}

func MiddlewareHandlerResponse(r *ghttp.Request) {
	r.Middleware.Next()

	// There's custom buffer content, it then exits current handler.
	if r.Response.BufferLength() > 0 {
		return
	}
	var (
		msg         = "操作成功！"
		ctx         = r.Context()
		err         = r.GetError()
		res         = r.GetHandlerResponse()
		code        = http.StatusOK
		resultModel = BaseResponse{}
	)
	if err != nil {
		code = http.StatusInternalServerError
		msg = err.Error()
		resultModel.Code = code
		resultModel.Type = "error"
	} else if r.Response.Status > 0 && r.Response.Status != http.StatusOK {
		msg = http.StatusText(r.Response.Status)
		switch r.Response.Status {
		case http.StatusNotFound:
			code = http.StatusNotFound
		case http.StatusForbidden:
			code = http.StatusNotFound
		}
		resultModel.Type = "warning"
	} else {
		resultModel.Type = "success"
	}
	resultModel.Code = code
	resultModel.Message = msg
	resultModel.Result = res
	internalErr := r.Response.WriteJson(resultModel)
	if internalErr != nil {
		g.Log().Fatalf(ctx, `%+v`, internalErr)
	}
}
