package controller

import (
	"fmt"
	"gen-code-v2/consts"
	"gen-code-v2/internal/model"
	"github.com/gogf/gf/v2/encoding/gjson"
	"github.com/gogf/gf/v2/frame/g"
	"github.com/gogf/gf/v2/net/ghttp"
	"github.com/gogf/gf/v2/os/gfile"
	"github.com/gogf/gf/v2/text/gstr"
)

var (
	AutoCode = cAutoCode{
		RegisterMap: make(map[string]map[string]*model.Methods, 0),
	}
)

type cAutoCode struct {
	RegisterMap map[string]map[string]*model.Methods
}

func (c *cAutoCode) Register(r *ghttp.Request) {
	module := r.Get("module").String()
	contents := gfile.GetContents(fmt.Sprintf("%s.yaml", module))
	json, err := gjson.LoadYaml(contents)
	if err != nil {
		return
	}
	register := new(model.Register)
	err = json.Scan(register)
	if err != nil {
		return
	}
	s := g.Server()
	err = g.Try(func() {
		methods := make(map[string]*model.Methods, 0)
		for _, method := range register.Methods {
			methods[method.Url] = method
			s.BindHandler(fmt.Sprintf("%s%s/%s", consts.AutoCode, module, method.Url), c.Crud)
		}
		c.RegisterMap[module] = methods
	})
	if err != nil {
		r.Response.WriteJsonExit(err)
	}
	r.Response.WriteJsonExit(fmt.Sprintf("%s模块注册成功", module))
}

func (c *cAutoCode) Crud(req *ghttp.Request) {
	url := gstr.Replace(req.RequestURI, consts.AutoCode, "")
	if index := gstr.PosI(url, consts.Slash); index > -1 {
		url = gstr.TrimLeft(url, consts.Slash)
	}
	module := gstr.StrTillEx(url, consts.Slash)
	url = gstr.TrimLeft(url, module+consts.Slash)
	url = gstr.StrTillEx(url, "?")
	methodMap := c.RegisterMap[module]
	var methods *model.Methods = methodMap[url]
	paramsObj := methods.Params
	dbName := methods.Db
	tableName := methods.Table
	pageFlag := methods.Page
	params := make(g.Map, 0)
	for _, param := range paramsObj {
		if !req.Get(param.Key).IsEmpty() {
			params[fmt.Sprintf("%s %s ?", param.Key, param.QueryType)] = fmt.Sprintf(param.Value, req.Get(param.Key).Interface())
		}
	}
	m := g.DB(dbName).Model(tableName)
	if pageFlag {
		pageNum := req.Get("pageNum").Int()
		pageSize := req.Get("pageSize").Int()
		if pageSize == 0 {
			pageSize = 10
		}
		m = m.Page(pageNum, pageSize)
	}
	result, errs := m.Where(params).All()
	if errs != nil {
		g.Log().Error(req.Context(), errs)
		return
	}
	if result.IsEmpty() {
		req.Response.WriteJsonExit(make([]interface{}, 0))
	}
	_ = req.Response.WriteJsonExit(result)
}
