package controller

import (
	"context"
	"fmt"
	"gen-code-v2/api/v1"
	"github.com/gogf/gf/v2/frame/g"
)

var (
	Index = cIndex{}
)

type cIndex struct{}

func (c *cIndex) GetInfo(ctx context.Context, req *v1.GetInfoReq) (res *v1.EmptyRes, err error) {
	jsonStr := `{"msg":"操作成功","code":200,"permissions":["*:*:*"],"roles":["admin"],"user":{"searchValue":null,"createBy":"admin","createTime":"2022-03-15 15:31:43","updateBy":null,"updateTime":null,"remark":"管理员","params":{},"userId":1,"deptId":103,"userName":"admin","nickName":"若依","email":"ry@163.com","phonenumber":"15888888888","sex":"1","avatar":"","salt":null,"status":"0","delFlag":"0","loginIp":"127.0.0.1","loginDate":"2022-03-15T15:31:43.000+08:00","dept":{"searchValue":null,"createBy":null,"createTime":null,"updateBy":null,"updateTime":null,"remark":null,"params":{},"deptId":103,"parentId":101,"ancestors":null,"deptName":"研发部门","orderNum":"1","leader":"若依","phone":null,"email":null,"status":"0","delFlag":null,"parentName":null,"children":[]},"roles":[{"searchValue":null,"createBy":null,"createTime":null,"updateBy":null,"updateTime":null,"remark":null,"params":{},"roleId":1,"roleName":"超级管理员","roleKey":"admin","roleSort":"1","dataScope":"1","menuCheckStrictly":false,"deptCheckStrictly":false,"status":"0","delFlag":null,"flag":false,"menuIds":null,"deptIds":null,"admin":true}],"roleIds":null,"postIds":null,"roleId":null,"admin":true}}`
	s := g.Server()
	routes := s.GetRoutes()
	fmt.Printf("ss:%v", routes)

	err = g.RequestFromCtx(ctx).Response.WriteJsonExit(jsonStr)

	return
}

func (c *cIndex) GetRouters(ctx context.Context, req *v1.GetRoutersReq) (res *v1.EmptyRes, err error) {
	jsonStr := `{"msg":"操作成功","code":200,"data":[{"redirect":"noRedirect","path":"/tool","component":"Layout","hidden":false,"children":[{"path":"gen","component":"tool/gen/index","hidden":false,"meta":{"noCache":false,"icon":"code","link":null,"title":"代码生成"},"name":"Gen"}],"meta":{"noCache":false,"icon":"tool","link":null,"title":"系统工具"},"name":"Tool","alwaysShow":true}]}`
	err = g.RequestFromCtx(ctx).Response.WriteJsonExit(jsonStr)
	return
}
