package cmd

import (
	"context"
	"gen-code-v2/internal/config"
	"gen-code-v2/internal/controller"
	"gen-code-v2/internal/middle"
	"gen-code-v2/internal/router"
	"github.com/gogf/gf/v2/frame/g"
	"github.com/gogf/gf/v2/net/ghttp"
	"github.com/gogf/gf/v2/os/gcmd"
)

var (
	Main = gcmd.Command{
		Name:  "main",
		Usage: "main",
		Brief: "start http server",
		Func: func(ctx context.Context, parser *gcmd.Parser) (err error) {
			config.InitLog(ctx)
			s := g.Server()
			s.Use(middle.MiddlewareHandlerResponse)
			s.Group("/", func(group *ghttp.RouterGroup) {
				group.Middleware(
					ghttp.MiddlewareCORS,
				)
				router.InitIndex(group) //index
				tool(group)
			})
			s.BindHandler("/register", controller.AutoCode.Register)

			s.Run()
			return nil
		},
	}
)

func tool(group *ghttp.RouterGroup) {
	group.Group("/tool", func(group *ghttp.RouterGroup) {
		router.InitDB(group)    //数据源
		router.InitTable(group) //表
		router.InitTpl(group)   //模板
	})
}
