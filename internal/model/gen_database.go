package model

type GenDatabaseReq struct {
	PageReq
	Group string // 分组名称
	Name  string // 数据库名称
}
