package model

type Register struct {
	Methods []*Methods `json:"methods"`
}

type Methods struct {
	Url       string                 `json:"url"`
	Params    []Param                `json:"Params"`
	Db        string                 `json:"db"`
	Table     string                 `json:"table"`
	Page      bool                   `json:"page"`
	Handle    string                 `json:"handle"`
	AddParams map[string]interface{} `json:"addParams"`
}

type Param struct {
	Key       string `json:"key"`
	QueryType string `json:"queryType"`
	Value     string `json:"value"`
}
